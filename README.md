## Installation ##

### Docker ###
1. Install [Docker](https://docs.docker.com/engine/installation/linux/ubuntu/)
2. Install [Docker Compose](https://docs.docker.com/compose/install/)
3. Run the project: `docker-compose up -d`
4. Install dependencies: `npm i`
5. Import MySql table: `mysql -uroot -h 0.0.0.0 testDb < ./sqldump.sql`

### Select data storage ###
You can use MySql or Google Firebase.
— For Firebase: Download firebase-credentials.json from https://console.firebase.google.com/
— For Mysql Import MySql table: `mysql -uroot -h 0.0.0.0 smartlock < 1_init.sql`

### Run Project ###
1. Build: `npm run build`
2. Start: `npm run start`
