export default {
  MYSQL_HOST: 'localhost',
  MYSQL_USER: 'user',
  MYSQL_PASSWORD: 'password',
  MYSQL_DATABASE: 'testDb',
  FORMAT_CHECKER_URL: 'http://localhost:3000',
  COMPROMISED_CHECKER_URL: 'http://localhost:5000',
  PASSWORDS_BATCH: 10,
};
