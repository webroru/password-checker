import mysql from 'mysql2/promise';
import config from '../config';
import CompromisedChecker from './checker/CompromisedChecker';
import FormatChecker from './checker/FormatChecker';
import PasswordMysqlRepository from './repository/PasswordMysqlRepository';

(async () => {
  console.log(`Start unchecked passwords processing`);

  const connection = await mysql.createConnection({
    host: config.MYSQL_HOST,
    user: config.MYSQL_USER,
    password: config.MYSQL_PASSWORD,
    database: config.MYSQL_DATABASE,
  });
  const passwordMysqlRepository = new PasswordMysqlRepository(connection);
  const formatChecker = new FormatChecker(config.FORMAT_CHECKER_URL);
  const compromisedChecker = new CompromisedChecker(
    config.COMPROMISED_CHECKER_URL,
  );
  formatChecker.setNext(compromisedChecker);
  let passwords = await passwordMysqlRepository.findUnvalidatedPasswords(
    config.PASSWORDS_BATCH,
  );
  let exitCode = 0;

  while (passwords.length) {
    await Promise.all(
      passwords.map(async (password) => {
        try {
          password.setValid(await formatChecker.handle(password.getPassword()));
          await passwordMysqlRepository.setValid(password);
        } catch (e) {
          console.error(e.message);
          exitCode = 1;
        }
      }),
    );
    passwords = await passwordMysqlRepository.findUnvalidatedPasswords(
      config.PASSWORDS_BATCH,
    );
  }

  console.log('Validation is done');
  process.exit(exitCode);
})();
