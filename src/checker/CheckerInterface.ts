export default interface CheckerInterface {
  setNext(handler: CheckerInterface): CheckerInterface;

  handle(password: string): Promise<boolean>;
}
