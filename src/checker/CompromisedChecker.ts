import fetch from 'node-fetch';
import AbstractChecker from './AbstractChecker';

export default class CompromisedChecker extends AbstractChecker {
  constructor(private readonly baseUrl: string) {
    super();
  }

  public async handle(password: string): Promise<boolean> {
    const result = await this.sendRequest(password);
    if (result?.message?.length) {
      return false;
    }

    return super.handle(password);
  }

  private async sendRequest(password: string): Promise<any> {
    const response = await fetch(
      `${this.baseUrl}/compromised?password=${password}`,
    );
    const body = await response.text();
    if (body) {
      return JSON.parse(body);
    }
  }
}
