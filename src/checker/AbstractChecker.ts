import CheckerInterface from './CheckerInterface';

export default abstract class AbstractChecker implements CheckerInterface {
  private nextChecker: CheckerInterface;

  public setNext(checker: CheckerInterface): CheckerInterface {
    this.nextChecker = checker;
    return checker;
  }

  public async handle(password: string): Promise<boolean> {
    if (this.nextChecker) {
      return this.nextChecker.handle(password);
    }

    return true;
  }
}
