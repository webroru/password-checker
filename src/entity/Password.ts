export default class Password {
  constructor(private password: string, private valid: boolean) {}

  public getPassword() {
    return this.password;
  }

  public setPassword(password: string) {
    this.password = password;
  }

  public isValid() {
    return this.valid;
  }

  public setValid(valid: boolean) {
    this.valid = valid;
  }
}
