import Password from 'src/entity/Password';

export default interface PasswordRepositoryInterface {
  findUnvalidatedPasswords(limit?: number): Promise<Password[]>;
  setValid(password: Password): Promise<void>;
}
