import mysql from 'mysql2/promise';
import Password from '../entity/Password';
import PasswordRepositoryInterface from './PasswordRepositoryInterface';

export default class PasswordMysqlRepository
  implements PasswordRepositoryInterface
{
  private readonly TABLE_NAME = 'passwords';
  constructor(private connection: mysql.Connection) {}

  async findUnvalidatedPasswords(limit?: number): Promise<Password[]> {
    let sql = `SELECT * FROM ${this.TABLE_NAME} WHERE valid IS NULL`;
    if (limit) {
      sql += ` LIMIT ${limit}`;
    }

    const [rows] = await this.connection.execute(sql);

    if (Array.isArray(rows)) {
      return rows.map((item) => new Password(item.password, item.valid));
    }

    return [];
  }

  async setValid(password: Password): Promise<void> {
    let isValid = null;
    if (password.isValid() === true) {
      isValid = 1;
    } else if (password.isValid() === false) {
      isValid = 0;
    }
    const sql = `UPDATE ${
      this.TABLE_NAME
    } SET valid = ${isValid} WHERE password = '${password.getPassword()}'`;
    await this.connection.execute(sql);
  }
}
